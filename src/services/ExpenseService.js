import axios from "axios";

const baseUrl = "http://localhost:5000/api/expense";

const createResponse = (isSuccess = true, error = null, payload = null) => {
  return {
    isSuccess,
    error,
    payload,
  };
};

/**
 * Make a call to expense api to add an expense
 * {@params Expense object}
 */
export const CreateExpense = async ({
  expenseId,
  name,
  amount,
  purchasedAt,
  description,
  tag,
  selectedCategory,
}) => {
  /*If exepnse id is present --> then update else add */
  var response = await axios.post(
    expenseId ? `${baseUrl}/update/${expenseId}` : `${baseUrl}/add`,
    {
      amount: parseFloat(amount),
      purchasedAt: new Date(purchasedAt),
      name,
      systemId: 1,
      createdBy: "user",
      categoryId: parseInt(selectedCategory),
      currencyId: 1,
      description,
      expenseId,
      tag,
    }
  );

  const wasSuccess = response.status >= 200 && response.status <= 299;
  return wasSuccess
    ? createResponse(true)
    : createResponse(false, response.statusText);
};

/*
 *Get expenses list based on page number and page size
 *Default page size and page numeber assigned if not specified .
 *{@ params pageNumber }
 *{@ params pageSize}
 */

export const GetExpenses = async (pageNumber = 1, pageSize = 10) => {
  const url = `${baseUrl}?pageNumber=${pageNumber}&pageSize=${pageSize}`;
  const response = await axios.get(url);

  return response.status >= 200 && response.status <= 299
    ? createResponse(true, null, response.data)
    : createResponse(false, response.statusText);
};

export const DeleteExpense = async (expenseId) => {
  const url = `${baseUrl}/${expenseId}`;
  const response = await axios.delete(url);

  return response.status >= 200 && response.status <= 299
    ? createResponse(true)
    : createResponse(false, response.statusText);
};

export const GetCategories = async () => {
  const url = "http://localhost:5000/api/category";
  const response = await axios.get(url);

  return response.status >= 200 && response.status <= 299
    ? createResponse(true, null, response.data)
    : createResponse(false, response.statusText);
};

export const Token = axios.CancelToken;
