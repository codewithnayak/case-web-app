import axios from "axios";

const Exists = async (name) => {
  const url = `http://localhost:7383/api/home/find/${name}`;

  try {
    const response = await axios.get(url);
    if (response.status >= 200 && response.status <= 299) return true;
    return true;
  } catch (e) {
    console.log(e.message);
    return false;
  }
};

export { Exists };
