import axios from "axios";
import bcrypt from "bcryptjs";

const Login = async (username, password) => {
  if (!password) throw new Error("password can not be null or empty ");
  if (!username) throw new Error("username can not be null or empty ");

  const salt = await bcrypt.genSalt(10);
  const hashedPassword = await bcrypt.hash(password, salt);
  const loginUrl = `${
    process.env.REACT_APP_IDENTITY_BASE_URL + process.env.REACT_APP_SIGNIN_URL
  }`;
  console.log(loginUrl);
  try {
    const response = await axios.post(loginUrl, {
      email: username,
      hashedPassword,
    });
    return {
      data: response.data,
      isSuccess: response.status === 200,
      error: response.statusText,
    };
  } catch (e) {
    return {
      data: null,
      isSuccess: false,
      error: e.message,
    };
  }
};

const Register = async (username, password) => {
  let errorMessage = "";
  const registerUrl = `${
    process.env.REACT_APP_IDENTITY_BASE_URL + process.env.REACT_APP_SIGNUP_URL
  }`;
  try {
    const response = await axios.post(registerUrl, {
      email: username,
      password,
    });
    if (response.status === 201) {
      return { success: true };
    }
  } catch (e) {
    errorMessage = e.message;
  }
  return {
    success: false,
    error: errorMessage,
  };
};

const Profile = async (email) => {
  try {
    const response = await axios.post(
      `${process.env.REACT_APP_IDENTITY_BASE_URL}${process.env.REACT_APP_PROFILE_URL}`,
      {
        email,
      }
    );

    if (response.status === 200) {
      return true;
    }
  } catch (e) {
    console.log(e.message);
  }
  return false;
};

const ForgotPassword = async (email) => {
  try {
    const response = await axios.post(
      `${process.env.REACT_APP_IDENTITY_BASE_URL}${process.env.REACT_APP_FORGOT_PASSWORD_URL}`,
      {
        email,
      }
    );

    if (response.status >= 200 && response.status <= 299) {
      return true;
    }
  } catch (e) {
    console.log(e.message);
  }
  return false;
};

export { Login, Register, Profile, ForgotPassword };
