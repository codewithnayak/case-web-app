import React from "react";

const Card = (props) => {
  const { name, description, sunscriptionType } = props;
  return (
    <>
      <div className="card ml-5" style={{ width: "18rem" }}>
        <i class="fab fa-elementor fa-3x"></i>
        <div className="card-body">
          <h5 className="card-title">{name}</h5>
          <p className="card-text">{description}</p>
          <a href="/manage" className="btn btn-primary">
            Go
          </a>
        </div>
      </div>
    </>
  );
};

export default Card;
