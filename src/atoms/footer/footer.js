import React from "react";

export default function Footer() {
  return (
    <div>
      <div className="border-top mt-1  navigation">
        <footer
          className="page-footer font-small blue"
          style={{
            color: "red",
          }}
        >
          <div className="footer-copyright text-center py-3">
            <small>© 2020: codewithnayak.com</small>
          </div>
        </footer>
      </div>
    </div>
  );
}
