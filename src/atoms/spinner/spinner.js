import React from "react";

export default function spinner({ message }) {
  console.log(message);
  return (
    <div className="d-flex justify-content-center">
      <div
        className="spinner-border text-info"
        role="status"
        style={{ width: "100px", height: "100px", marginTop: "300px" }}
      >
        <span className="sr-only">{message}</span>
      </div>
    </div>
  );
}
