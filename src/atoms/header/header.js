import React from "react";

export default function header() {
  return (
    <div>
      <div className="container-fluid">
        <nav className="navbar navbar-dark bg-danger border-bottom navigation">
          <span className="navbar-brand mb-0 h1">
            <i className="fas fa-home fa-1x"></i>asiana
          </span>
        </nav>
      </div>
    </div>
  );
}
