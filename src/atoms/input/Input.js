import React from "react";
import PropTypes from "prop-types";

const Input = (props) => {
  return (
    <>
      <input
        name={props.name}
        type={props.type}
        className={props.className}
        id={props.id}
        onChange={props.handleChange}
        value={props.value}
        onBlur={props.handleBlur}
      />
    </>
  );
};

Input.propTypes = {
  name: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  className: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  handleChange: PropTypes.func.isRequired,
  value: PropTypes.string.isRequired,
};

export default Input;
