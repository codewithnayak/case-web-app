import React from "react";
import Input from "../atoms/input/Input";

const Login = (props) => {
  const { email, password, isValidEmail, handleSubmit, handleChange } = props;

  return (
    <>
      <div>
        <form onSubmit={handleSubmit}>
          <div className="form-group">
            <label htmlFor="email">
              {" "}
              <b>Email</b>{" "}
            </label>
            <Input
              name="email"
              type="email"
              className={`form-control ${
                email.length > 0
                  ? isValidEmail
                    ? "is-valid"
                    : "is-invalid"
                  : null
              } border`}
              id="email"
              aria-describedby="emailHelp"
              handleChange={handleChange}
              value={email}
            />

            <div className="invalid-feedback">Please enter a valid email </div>
          </div>
          <div className="form-group">
            <label htmlFor="password">
              <b>Password</b>
            </label>
            <Input
              name="password"
              type="password"
              className={`form-control ${
                password.length > 0 ? "is-valid" : null
              } border`}
              id="password"
              handleChange={handleChange}
              value={password}
            />
          </div>
          <button
            type="submit"
            className="btn btn-danger"
            disabled={!(isValidEmail && password.length > 0)}
          >
            Login
          </button>
        </form>
      </div>
    </>
  );
};

export default Login;
