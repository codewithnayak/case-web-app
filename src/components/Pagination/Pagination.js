import React from "react";

export default function Pagination({ pageList, handlePageClick, currentPage }) {
  return (
    <div>
      <nav aria-label="...">
        <ul className="pagination ">
          <li className="page-item disabled">
            <button className="page-link" href="#" tabIndex="-1">
              Previous
            </button>
          </li>
          {pageList.map((page) => {
            const active =
              currentPage === page ? "page-item active" : "page-item";
            return (
              <li className={active} key={page}>
                <button
                  className="page-link"
                  href="#"
                  onClick={() => handlePageClick(page)}
                >
                  {page}{" "}
                </button>
              </li>
            );
          })}
          <li className="page-item">
            <button className="page-link" href="#">
              Next
            </button>
          </li>
        </ul>
      </nav>
    </div>
  );
}
