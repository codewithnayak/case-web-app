import Input from "../../atoms/input/Input";

const Register = (props) => {
  console.log("props" + props);
  const {
    email,
    password,
    confirmPassword,
    handleChange,
    feedback,
    disableSubmit,
    handleBlur,
    handleSubmit,
    policySuccess,
    existingMember,
  } = props;

  const emailCss = (isPrestine, invalid) => {
    return `form-control ${
      isPrestine.length > 0 ? (invalid ? "is-invalid" : "is-valid") : null
    } border`;
  };
  const ExistingMember = (props) => {
    return (
      <>
        <div>
          <span style={{ color: "red" }}>
            {props.alert} {props.isMember && <a href="/">Login</a>}
          </span>
        </div>
      </>
    );
  };
  return (
    <>
      <form onSubmit={handleSubmit}>
        <div className="form-group">
          <label htmlFor="email">
            <b>Email</b>{" "}
          </label>
          <Input
            name="email"
            type="email"
            className={emailCss(email, feedback)}
            id="email"
            aria-describedby="emailHelp"
            handleBlur={handleBlur}
            handleChange={handleChange}
            value={email}
          />
          <small id="emailHelp" className="form-text text-muted">
            We'll never share your email with anyone else.
          </small>
          <div className="invalid-feedback">
            {<ExistingMember alert={feedback} isMember={existingMember} />}
          </div>
        </div>
        <div className="form-group">
          <label htmlFor="password">
            <b>Password</b>
          </label>
          <Input
            name="password"
            type="password"
            className={`form-control ${emailCss(
              password,
              !policySuccess
            )} border`}
            id="password"
            handleChange={handleChange}
            value={password}
          />
          <small id="emailHelp" className="form-text text-muted">
            Minimum 6 charecters.One upper case , one number and one lower case
          </small>
        </div>
        <div className="form-group">
          <label htmlFor="confirmPassword">
            <b>Confirm Password</b>
          </label>
          <Input
            name="confirmPassword"
            type="password"
            className={emailCss(confirmPassword, confirmPassword !== password)}
            id="confirmPassword"
            handleChange={handleChange}
            value={confirmPassword}
          />
          <div className="invalid-feedback">Password should match</div>
        </div>
        <button
          type="submit"
          className="btn btn-danger"
          disabled={disableSubmit}
        >
          Sign up
        </button>
      </form>
    </>
  );
};

export default Register;
