import React from "react";

export default function SingleExpense(props) {
  const { id, name, amount, handleDelete, handleEdit, date } = props;

  return (
    <>
      <div className="row">
        <div className="col">{name}</div>
        <div className="col">£{amount}</div>
        <div className="col">{new Date(date).toLocaleDateString("en-GB")}</div>
        <div className="col">
          <button className="btn btn-primary" onClick={() => handleEdit(id)}>
            <i className="fas fa-edit"></i>
          </button>
        </div>
        <div className="col">
          <button className="btn btn-danger" onClick={() => handleDelete(id)}>
            <i className="fa fa-trash" aria-hidden="true"></i>
          </button>
        </div>
      </div>
    </>
  );
}
