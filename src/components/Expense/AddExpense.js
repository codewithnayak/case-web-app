import React from "react";
import DatePicker from "react-datepicker";
import { Modal, Button } from "react-bootstrap";
import "react-datepicker/dist/react-datepicker.css";

export default function AddExpense({
  name,
  amount,
  purchasedAt,
  description,
  handleChange,
  handleSubmit,
  handleDateChange,
  show,
  handleClose,
  tag,
  createUpdateText,
  validation,
  formValid,
  categories,
}) {
  const formCss = ({ prestine, invalid }) => {
    if (prestine) return " form-control border ";
    return invalid
      ? "form-control is-invalid border "
      : "form-control is-valid border  ";
  };

  return (
    <Modal show={show} onHide={() => handleClose(false)}>
      <Modal.Header closeButton>
        <Modal.Title>{createUpdateText} Expense</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="container">
          <form>
            <div className="form-group">
              <label htmlFor="exampleFormControlInput1">Name</label>
              <input
                type="text"
                className={formCss(validation.name)}
                id="exampleFormControlInput1"
                name="name"
                value={name}
                onChange={handleChange}
              />
              <div className="invalid-feedback">{validation.name.message}</div>
            </div>
            <div className="form-group">
              <label htmlFor="exampleFormControlInput1">Amount</label>
              <input
                type="text"
                className={formCss(validation.amount)}
                id="exampleFormControlInput1"
                name="amount"
                value={amount}
                onChange={handleChange}
              />
              <div className="invalid-feedback">
                {validation.amount.message}
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-6">
                <label htmlFor="purchasedAt">PurchasedOn</label>
                <DatePicker
                  id="purchasedAt"
                  className={formCss(validation.purchasedAt)}
                  selected={purchasedAt}
                  name="purchasedAt"
                  value={purchasedAt}
                  onChange={(date) => handleDateChange(date)}
                />
                <div className="invalid-feedback">
                  {validation.purchasedAt.message}
                </div>
              </div>
              <div className="form-group col-md-6">
                <label htmlFor="purchasedAt">Tag</label>
                <input
                  type="text"
                  className={formCss(validation.tag)}
                  id="exampleFormControlInput1"
                  name="tag"
                  value={tag}
                  onChange={handleChange}
                />
                <div className="invalid-feedback">{validation.tag.message}</div>
                <small>Tag it with a friendly name</small>
              </div>
            </div>
            <div className="form-group">
              <label htmlFor="exampleFormControlSelect1">Category</label>
              <select
                name="selectedCategory"
                className={formCss(validation.category)}
                id="exampleFormControlSelect1"
                onChange={handleChange}
              >
                {categories.map(({ id, categoryName }) => {
                  return (
                    <option key={id} value={id}>
                      {categoryName}
                    </option>
                  );
                })}
              </select>
              <div className="invalid-feedback">
                {validation.category.message}
              </div>
            </div>

            <div className="form-group">
              <label htmlFor="exampleFormControlTextarea1">Description</label>
              <textarea
                className="form-control"
                id="exampleFormControlTextarea1"
                rows="3"
                name="description"
                value={description}
                onChange={handleChange}
              ></textarea>
            </div>
          </form>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={handleClose}>
          Close
        </Button>
        <Button variant="danger" onClick={handleSubmit} disabled={!formValid}>
          {createUpdateText}
        </Button>
      </Modal.Footer>
    </Modal>
  );
}
