import "./App.css";
import React from "react";
import { Route } from "react-router-dom";
import LoginContainer from "./containers/Login";
import HomeContainer from "./containers/Home/Home";
import RegsiterContainer from "./containers/Register/register";
import Expense from "./containers/Expense/Expense";

import Header from "./atoms/header/header";
import Footer from "./atoms/footer/footer";
import ForgotPassword from "./pages/forgotPassword/ForgotPassword";
import ResetPassword from "./pages/resetPassword/resetPassword";
import CreateHome from "./pages/createHome/CreateHome";

/*TODO: [DH-8]Use switch case on router*/

function App() {
  const renderApp = () => {
    return (
      <>
        <div>
          <Route path="/" exact>
            <LoginContainer />
          </Route>
          <Route path="/home">
            <Header />
            <HomeContainer />
            <Footer />
          </Route>
          <Route path="/signup">
            <RegsiterContainer />
          </Route>
          <Route path="/expense">
            <Header />
            <Expense />
            <Footer />
          </Route>
          <Route path="/forgot-password">
            <Header />
            <ForgotPassword />
            <Footer />
          </Route>
          <Route path="/reset-password/:secret">
            <Header />
            <ResetPassword />
            <Footer />
          </Route>
          <Route path="/create-home">
            <Header />
            <CreateHome />
            <Footer />
          </Route>
        </div>
      </>
    );
  };

  return <>{renderApp()}</>;
}

export default App;
