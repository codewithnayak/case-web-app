export const isEmail = (email) => {
  if (!email || typeof email !== "string") return false;
  const emailFormat = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
  return email.match(emailFormat) ? true : false;
};

/*
Checks whether password is strong or not 
-- Must be 6 charecters or long
-- Must has an upper case 
-- Must has a lower case 
-- Must has a number
*/
export const passwordPolicyMatched = (password) => {
  if (password.length < 6) return false;

  const hasUpperCase = /[A-Z]/.test(password);
  const hasLowerCase = /[a-z]/.test(password);
  const hasNumbers = /\d/.test(password);

  return hasUpperCase && hasLowerCase && hasNumbers;
};
