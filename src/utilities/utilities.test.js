import { isEmail, passwordPolicyMatched } from "./utilities";

describe("email check", () => {
  it("wrong email check", () => {
    const isValid = isEmail("dasdasdas");
    expect(isValid).toBe(false);
  });
  it("correct email check", () => {
    const isValid = isEmail("someone@example.com");
    expect(isValid).toBe(true);
  });
  it("when nothing is given should return false", () => {
    const isValid = isEmail(null);
    expect(isValid).toBe(false);
  });
  it("when input is not string should return false", () => {
    const isValid = isEmail(12312321);
    expect(isValid).toBe(false);
  });
});

describe("password strength check", () => {
  it("strong password", () => {
    const isStrong = passwordPolicyMatched("Mainframe123");
    expect(isStrong).toBe(true);
  });
  it("weak passwords", () => {
    const passwordInputs = ["abc", "abcd123", "adsdwww"];
    for (let i = 0; i < passwordInputs.length; i++) {
      const element = passwordInputs[i];
      const isStrong = passwordPolicyMatched(element);
      expect(isStrong).toBe(false);
    }
  });
});
