import React, { useState } from "react";
import Notification from "../notification/notification";
import { Form, Button, FormControl, Col } from "react-bootstrap";
import { Exists } from "../../services/DigitalHomeManager";
import Address from "../Address/Address";
import AddressSearch from "../Address/AddressSearch";
export default function CreateHome() {
  const [notification, setNotiifcation] = useState("");
  const [name, setName] = useState("");
  const [postCode, setPostCode] = useState("");
  const [showAddress, setShowAddress] = useState(true);
  const [nameError, setNameError] = useState("");

  const [showAddressSeacrh, setShowAddressSearch] = useState(true);

  const handleSubmit = () => {};

  const handleBlur = async (e) => {
    const { value } = e.target;
    const exists = await Exists(value);
    console.log(exists);
    if (exists) return;
    setNameError("Already taken .Please choose another name .");
  };

  return (
    <>
      <div
        className="container mt-1 shadow p-3 mb-1 bg-white rounded hoverable"
        style={{
          height: "750px",
        }}
      >
        <Notification {...notification} setMessage={setNotiifcation} />
        <div className="row mt-5">
          <div className="col"></div>
          <div className="col-6">
            <div className="form-group">
              <Form onSubmit={handleSubmit}>
                <Form.Group>
                  <Form.Label>
                    <b>Name</b>
                  </Form.Label>
                  <Form.Control
                    name="name"
                    value={name}
                    onChange={(e) => setName(e.target.value)}
                    onBlur={handleBlur}
                    className={`form-control ${
                      name.length > 0
                        ? nameError
                          ? "is-invalid"
                          : "is-valid"
                        : null
                    }`}
                  />
                  <Form.Text className="text-muted">
                    Give your digital home a beautiful name
                  </Form.Text>
                  <FormControl.Feedback type="invalid">
                    {nameError}
                  </FormControl.Feedback>
                </Form.Group>

                {showAddressSeacrh && <AddressSearch />}

                <Button variant="danger" disabled>
                  Create
                </Button>
              </Form>
            </div>
          </div>
          <div className="col"></div>
        </div>
      </div>
    </>
  );
}
