import React, { useState } from "react";
import { Form, Button, FormControl } from "react-bootstrap";
import { isEmail } from "../../utilities/utilities";
import {
  Profile,
  ForgotPassword as FPassword,
} from "../../services/AuthService";
import { Link } from "react-router-dom";
import Notification from "../notification/notification";

const notificationState = {
  isError: false,
  message: "",
};

export default function ForgotPassword() {
  const [notification, setNotiifcation] = useState(notificationState);
  const [email, setEmail] = useState("");
  const [error, setError] = useState("");
  const [formValid, setFormValid] = useState(false);
  const emailCss = (isPrestine, invalid) => {
    return `form-control ${
      isPrestine.length > 0 ? (invalid ? "is-invalid" : "is-valid") : null
    } border`;
  };

  const handleChange = ({ value }) => {
    setEmail(value);
    const message = `${!isEmail(value) ? "Please enter a valid email" : ""}`;
    setError(message);
  };
  const handleBlur = async ({ value }) => {
    if (!isEmail(value)) {
      return;
    }
    const response = await Profile(value);

    if (!response) {
      setFormValid(false);
      setError(
        "We could not recognise the email .Please register if you are not a user "
      );
      return;
    }

    setFormValid(true);
  };

  const handleClick = async () => {
    const success = await FPassword(email);

    setNotiifcation({
      isError: !success,
      message: success
        ? "An email has been sent successfully.Please check for further instructions "
        : "An error occurred ",
    });
  };

  return (
    <>
      <div
        className="container mt-1 shadow p-3 mb-1 bg-white rounded"
        style={{
          height: "750px",
        }}
      >
        <Notification {...notification} setMessage={setNotiifcation} />
        <div className="row mt-5">
          <div className="col"></div>
          <div className="col-6">
            <Form.Label htmlFor="email">
              <b>Email</b>
            </Form.Label>
            <Form.Control
              name="email"
              type="text"
              id="email"
              aria-describedby="passwordHelpBlock"
              className={emailCss(email, error)}
              onChange={(e) => handleChange(e.target)}
              onBlur={(e) => handleBlur(e.target)}
              value={email}
            />
            <FormControl.Feedback type="invalid">{error}</FormControl.Feedback>
            <Form.Text id="passwordHelpBlock" muted>
              If you are a member , check your inbox for further intstruction.
              <Link to="/signup">Register</Link>
            </Form.Text>
            <Button
              variant="danger"
              disabled={!formValid}
              onClick={handleClick}
            >
              Send
            </Button>
          </div>
          <div className="col"></div>
        </div>
      </div>
    </>
  );
}
