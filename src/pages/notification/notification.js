import React from "react";

export default function Notification({ isError, message, setMessage }) {
  return (
    <>
      {message && (
        <div
          className={`alert alert-${
            isError ? "danger" : "success"
          } alert-dismissible fade show`}
          role="alert"
        >
          <strong>{isError ? "Error" : "Success"}:</strong> {message}
          <button
            onClick={() => setMessage({ message: "" })}
            type="button"
            className="close"
            data-dismiss="alert"
            aria-label="Close"
          >
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      )}
    </>
  );
}
