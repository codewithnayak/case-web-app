import React, { useState, useEffect } from "react";
import { Form, Button, FormControl } from "react-bootstrap";
import { passwordPolicyMatched } from "../../utilities/utilities";
import { Profile, ForgotPassword } from "../../services/AuthService";
import { useParams, useHistory } from "react-router-dom";
import Notification from "../notification/notification";

const initialState = {
  password: "",
  confirmPassword: "",
  passwordError: "",
  confirmPasswordError: "",
  isFormValid: false,
};

export default function ResetPassword(props) {
  const { secret } = useParams();
  const [passwordForm, setPasswordForm] = useState(initialState);
  const [notification, setNotiifcation] = useState("");
  const history = useHistory();
  const handleChange = ({ name, value }) => {
    switch (name) {
      case "password":
        const isPolicyMatched = passwordPolicyMatched(value);
        setPasswordForm({
          ...passwordForm,
          [name]: value,
          passwordError: `${
            isPolicyMatched ? "" : "Adhere to password policy "
          }`,
          isFormValid:
            isPolicyMatched &&
            passwordForm.password === passwordForm.confirmPassword,
        });
        break;
      case "confirmPassword":
        const matched = passwordForm.password === value;
        setPasswordForm({
          ...passwordForm,
          [name]: value,
          confirmPasswordError: `${matched ? "" : "Should match"}`,
          isFormValid: passwordPolicyMatched(passwordForm.password) && matched,
        });
        break;
      default:
        break;
    }
  };

  const handleBlur = ({ name, value }) => {
    switch (name) {
      case "password":
        const isPolicyMatched = passwordPolicyMatched(value);
        setPasswordForm({
          ...passwordForm,
          [name]: value,
          passwordError: `${
            isPolicyMatched ? "" : "Adhere to password policy "
          }`,
          confirmPasswordError:
            value === passwordForm.confirmPassword ? "" : "Should match",
          isFormValid:
            isPolicyMatched &&
            passwordForm.password === passwordForm.confirmPassword,
        });
        break;
      case "confirmPassword":
        const matched = passwordForm.password === value;
        setPasswordForm({
          ...passwordForm,
          [name]: value,
          passwordError: passwordPolicyMatched(passwordForm.password)
            ? ""
            : "Adhere to password policy.",
          confirmPasswordError: `${matched ? "" : "Should match"}`,
          isFormValid: passwordPolicyMatched(passwordForm.password) && matched,
        });
        break;
      default:
        break;
    }
  };
  const handleClick = () => {
    history.push("/");
    //redirect to login after successful password change
  };

  useEffect(() => {
    console.log(secret);
  }, [secret]);

  const emailCss = (isPrestine, invalid) => {
    return `form-control ${
      isPrestine.length > 0 ? (invalid ? "is-invalid" : "is-valid") : null
    } border`;
  };

  return (
    <>
      <div
        className="container mt-1 shadow p-3 mb-1 bg-white rounded hoverable"
        style={{
          height: "750px",
        }}
      >
        <Notification {...notification} setMessage={setNotiifcation} />
        <div className="row mt-5">
          <div className="col"></div>
          <div className="col-6">
            <div className="form-group">
              <Form.Label htmlFor="password">
                <b>Password</b>
              </Form.Label>
              <Form.Control
                name="password"
                type="password"
                id="email"
                aria-describedby="passwordHelpBlock"
                className={emailCss(
                  passwordForm.password,
                  passwordForm.passwordError
                )}
                onChange={(e) => handleChange(e.target)}
                onBlur={(e) => handleBlur(e.target)}
                value={passwordForm.password}
              />
              <FormControl.Feedback type="invalid">
                {passwordForm.passwordError}
              </FormControl.Feedback>
              <Form.Text id="passwordHelpBlock" muted>
                If you are a member , check your inbox for further intstruction.
              </Form.Text>
            </div>
            <div className="form-group">
              <Form.Label htmlFor="password">
                <b>Confirm password</b>
              </Form.Label>
              <Form.Control
                name="confirmPassword"
                type="password"
                id="confirmPassword"
                aria-describedby="passwordHelpBlock"
                className={emailCss(
                  passwordForm.confirmPassword,
                  passwordForm.confirmPasswordError
                )}
                onChange={(e) => handleChange(e.target)}
                onBlur={(e) => handleBlur(e.target)}
                value={passwordForm.confirmPassword}
              />
              <FormControl.Feedback type="invalid">
                {passwordForm.confirmPasswordError}
              </FormControl.Feedback>
              <Form.Text id="passwordHelpBlock" muted></Form.Text>
            </div>
            <Button
              variant="danger"
              disabled={!passwordForm.isFormValid}
              onClick={handleClick}
            >
              Send
            </Button>
          </div>
          <div className="col"></div>
        </div>
      </div>
    </>
  );
}
