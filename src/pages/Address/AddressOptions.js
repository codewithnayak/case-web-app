import React from "react";
import { ListGroup } from "react-bootstrap";
export default function AddressOptions(props) {
  return (
    <div>
      <ListGroup>
        {props.addresses.map(({ id, address }) => {
          return (
            <ListGroup.Item action onClick={props.handleAdressClick} key={id}>
              {address}
            </ListGroup.Item>
          );
        })}
      </ListGroup>
    </div>
  );
}
