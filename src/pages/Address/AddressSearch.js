import React, { useState } from "react";
import { Form, FormControl } from "react-bootstrap";
import AddressOptions from "../Address/AddressOptions";
import axios from "axios";
import Address from "../Address/Address";

export default function AddressSearch() {
  const [postCode, setPostCode] = useState("");
  const [postCodeError, setPostCodeError] = useState("");
  const [suggestions, setAddressSuggestions] = useState([]);
  const [showAddress, setShowAddress] = useState(false);

  const handleChange = async ({ name, value }) => {
    setPostCode(value);
    if (value.length <= 3) return;
    var response = await axios.get("http://localhost:7000/suggesstions");
    console.log(response.data);
    setAddressSuggestions(response.data);
  };

  const handleAdressClick = (addressId) => {
    setAddressSuggestions([]);
    setShowAddress(true);
  };

  return (
    <>
      <Form.Group>
        <Form.Label>
          <b>PostCode</b>
        </Form.Label>
        <Form.Control
          name="postCode"
          value={postCode}
          onChange={(e) => handleChange(e.target)}
          onBlur={null}
          className={`form-control ${
            postCode.length > 0
              ? postCodeError
                ? "is-invalid"
                : "is-valid"
              : null
          }`}
        />
        {
          <AddressOptions
            addresses={suggestions}
            handleAdressClick={handleAdressClick}
          />
        }
        <FormControl.Feedback type="invalid">
          {postCodeError}
        </FormControl.Feedback>
      </Form.Group>
      {showAddress && <Address />}
    </>
  );
}
