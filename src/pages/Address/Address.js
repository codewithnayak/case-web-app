import React from "react";
import { Form, Button, FormControl, Col } from "react-bootstrap";

export default function Address() {
  return (
    <>
      <Form.Row>
        <Form.Group as={Col} controlId="formGridEmail">
          <Form.Label>Building Number</Form.Label>
          <Form.Control type="email" placeholder="Building number" />
        </Form.Group>

        <Form.Group as={Col} controlId="formGridPassword">
          <Form.Label>Building Name</Form.Label>
          <Form.Control type="password" placeholder="Building name" />
        </Form.Group>
      </Form.Row>

      <Form.Group controlId="formGridAddress1">
        <Form.Label>Address</Form.Label>
        <Form.Control placeholder="1234 Main St" />
      </Form.Group>

      <Form.Group controlId="formGridAddress2">
        <Form.Label>Address 2</Form.Label>
        <Form.Control placeholder="Apartment, studio, or floor" />
      </Form.Group>

      <Form.Row>
        <Form.Group as={Col} controlId="formGridCity">
          <Form.Label>City</Form.Label>
          <Form.Control />
        </Form.Group>

        <Form.Group as={Col} controlId="formGridState">
          <Form.Label>State/Province</Form.Label>
          <Form.Control as="select" defaultValue="--Select--">
            <option>--Select--</option>
            <option>Wiltshire</option>
          </Form.Control>
        </Form.Group>

        <Form.Group as={Col} controlId="formGridState">
          <Form.Label>Country</Form.Label>
          <Form.Control as="select" defaultValue="--Select--">
            <option>--Select--</option>
            <option>United Kingdom</option>
          </Form.Control>
        </Form.Group>
      </Form.Row>
    </>
  );
}
