import React, { useState } from "react";
import Register from "../../components/Register/register.js";
import { isEmail, passwordPolicyMatched } from "../../utilities/utilities";
import { Profile, Register as SignUp } from "../../services/AuthService";
import { useHistory } from "react-router-dom";

const initialState = {
  email: "",
  password: "",
  confirmPassword: "",
  feedback: "",
  disableSubmit: true,
  policySuccess: false,
  existingMember: false,
};

const RegsiterContainer = () => {
  const [formValue, setFormValue] = useState(initialState);
  const history = useHistory();
  const handleSubmit = async (e) => {
    e.preventDefault();
    const { email, password } = formValue;
    const response = await SignUp(email, password);
    if (response.success) {
      history.push("/home");
    } else {
      console.log("Error occurred");
    }
  };

  const handleBlur = async (e) => {
    if (!isEmail(e.target.value)) return;
    const response = await Profile(e.target.value);
    if (response) {
      setFormValue({
        ...formValue,
        feedback: "Already Member.Try logging in.",
        disableSubmit: true,
        existingMember: true,
      });
    } else {
      setFormValue({
        ...formValue,
        disableSubmit: false,
        existingMember: false,
      });
    }
  };

  const feedbackMessage = (value) =>
    !isEmail(value) ? "Please enter a valid email " : "";

  const handleChange = (e) => {
    const { name, value } = e.target;

    switch (name) {
      case "email":
        setFormValue({
          ...formValue,
          [name]: value,
          feedback: feedbackMessage(value),
          existingMember: false,
          disableSubmit:
            feedbackMessage(value).length > 0 ||
            !formValue.policySuccess ||
            formValue.password !== formValue.confirmPassword ||
            formValue.existingMember,
        });
        break;
      case "password":
        setFormValue({
          ...formValue,
          [name]: value,
          policySuccess: passwordPolicyMatched(value),
          disableSubmit:
            formValue.feedback.length > 0 ||
            !formValue.policySuccess ||
            value !== formValue.confirmPassword ||
            formValue.existingMember,
        });
        break;
      case "confirmPassword":
        setFormValue({
          ...formValue,
          [name]: value,
          disableSubmit:
            formValue.feedback.length > 0 ||
            !formValue.policySuccess ||
            formValue.password !== value ||
            formValue.existingMember,
        });
        break;
      default:
        break;
    }

    console.log(formValue);
  };

  return (
    <>
      <div
        className="container mt-1 shadow p-3 mb-1 bg-white rounded"
        style={{
          height: "750px",
        }}
      >
        <div className="row ">
          <div className="col-md ">
            <nav className="navbar navbar-dark  bg-danger border-bottom navigation">
              <span className="navbar-brand mb-0 h1">
                <i className="fas fa-home fa-1x"></i>asiana
              </span>
            </nav>
          </div>
        </div>

        <div
          style={{
            height: "550px",
          }}
        >
          <div className="row mt-1 ">
            <div className="col"></div>
            <div className="col-6 mb-5 mt-5">
              <div className="row mb-3">
                <div className="col-12">
                  <Register
                    {...formValue}
                    handleSubmit={handleSubmit}
                    handleBlur={handleBlur}
                    handleChange={handleChange}
                  />
                </div>
              </div>
              <div className="row mt-5 border-top">
                <div className="col"></div>

                <div className="col">
                  <small>
                    Already a member?
                    <a href="/" style={{ color: "#f01f0e" }}>
                      {" "}
                      Sign in
                    </a>
                  </small>
                </div>
              </div>
            </div>
            <div className="col"></div>
          </div>
        </div>
        <div>
          <div className="border-top mt-1  navigation">
            <footer
              className="page-footer font-small blue"
              style={{
                color: "red",
              }}
            >
              <div className="footer-copyright text-center py-3">
                <small>© 2020: codewithnayak.com</small>
              </div>
            </footer>
          </div>
        </div>
      </div>
    </>
  );
};

export default RegsiterContainer;
