import React, { useState, useEffect } from "react";
import axios from "axios";
import Card from "../../atoms/cards/card";
import "./Home.css";

const HomeContainer = () => {
  const [features, setFeatures] = useState([]);

  useEffect(() => {
    axios
      .get("http://localhost:7000/features")
      .then((res) => setFeatures(res.data))
      .catch((err) => console.log(err));
  }, []);
  return (
    <>
      <div className="box">
        {features.map((feature) => {
          return (
            <div key={feature.id}>
              <Card {...feature} />
            </div>
          );
        })}
      </div>
    </>
  );
};

export default HomeContainer;

//call features and populate the avilable features cards
