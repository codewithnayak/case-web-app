import React from "react";

export default function ListExpense({
  expenses,
  handleEdit,
  handleDelete,
  categories,
}) {
  return (
    <div>
      <table className="table table-borderless ">
        <thead>
          <tr>
            <th scope="col">Name</th>
            <th scope="col">PurchasedOn</th>
            <th scope="col">Amount</th>
            <th scope="col">Category</th>
            <th scope="col">Description</th>
            <th scope="col">Spender</th>
            <th scope="col">Edit</th>
            <th scope="col">Delete</th>
          </tr>
        </thead>
        <tbody>
          {expenses.map(
            ({
              name,
              expenseId,
              description,
              amount,
              purchasedAt,
              createdBy,
              categoryId,
            }) => {
              return (
                <tr key={expenseId}>
                  <td>{name}</td>
                  <td>{new Date(purchasedAt).toLocaleDateString("en-GB")}</td>
                  <td>${amount}</td>
                  <td>
                    {
                      categories.find((val) => val.id === categoryId)
                        .categoryName
                    }
                  </td>
                  <td>{description}</td>
                  <td>{createdBy}</td>
                  <td>
                    <span
                      className="edit"
                      onClick={() => handleEdit(expenseId)}
                    >
                      <i className="fas fa-edit"></i>
                    </span>
                  </td>
                  <td>
                    <span
                      className="delete"
                      onClick={() => handleDelete(expenseId)}
                    >
                      <i className="fa fa-trash" aria-hidden="true"></i>
                    </span>
                  </td>
                </tr>
              );
            }
          )}
        </tbody>
      </table>
    </div>
  );
}
