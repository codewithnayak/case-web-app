import React, { useState, useEffect } from "react";
import AddExpense from "../../components/Expense/AddExpense";
import "./Expense.css";
import Pagination from "../../components/Pagination/Pagination";
import {
  CreateExpense,
  GetExpenses,
  DeleteExpense,
  GetCategories,
  Token,
} from "../../services/ExpenseService";
import ListExpense from "./ListExpense";

const initialState = {
  currentPage: 1,
  previousPage: null,
  totalPages: 0,
  nextPage: null,
  data: [],
};

const initialExpenseData = {
  name: "",
  amount: "",
  purchasedAt: new Date(),
  description: "",
  createUpdateText: "Create",
  tag: "",
  selectedCategory: 0,
  validation: {
    name: {
      prestine: true,
      invalid: false,
      message: "Name is required ",
    },
    amount: {
      prestine: true,
      invalid: false,
      message: "Amount is required ",
    },
    tag: {
      prestine: true,
      invalid: false,
      message: "Tag is required ",
    },
    category: {
      prestine: true,
      invalid: false,
      message: "Select a category ",
    },
    purchasedAt: {
      prestine: true,
      invalid: false,
      message: "Purchase date  is required ",
    },
  },
  formValid: false,
};

const initialNotification = {
  message: "",
  isError: false,
};

export default function Expense() {
  const [expenseData, setExpenseData] = useState(initialExpenseData);
  const [expenses, setExpenses] = useState(initialState);
  const [pageList, setPageList] = useState([]);
  const [showModal, setShowModal] = useState(false);
  const [notification, setNotofocation] = useState(initialNotification);
  const [categories, setCategories] = useState([]);

  const handleClose = () => setShowModal(false);

  const handleDateChange = (selectedDate) => {
    setExpenseData({
      ...expenseData,
      purchasedAt: selectedDate,
      validation: {
        ...expenseData.validation,
        purchasedAt: {
          prestine: false,
          invalid: selectedDate ? false : true,
          message: "",
        },
      },
    });
  };

  const formValid = (validation) => {
    return !(
      validation.name.invalid ||
      validation.amount.invalid ||
      validation.tag.invalid
    );
  };

  const handleExpenseChange = (e) => {
    const { name, value } = e.target;
    switch (name) {
      case "name":
        setExpenseData({
          ...expenseData,
          [name]: value,
          validation: {
            ...expenseData.validation,
            [name]: {
              prestine: false,
              invalid: false,
              message: "",
            },
          },
          formValid: formValid(expenseData.validation),
        });
        break;
      case "amount":
        const isValid = /^\d*\.?\d*$/.test(value) && value > 0;
        console.log(isValid);
        setExpenseData({
          ...expenseData,
          [name]: value,
          validation: {
            ...expenseData.validation,
            [name]: {
              invalid: !isValid,
              message: "Should be a number and greater than zero",
            },
          },
        });
        break;
      case "tag":
        setExpenseData({
          ...expenseData,
          [name]: value,
          validation: {
            ...expenseData.validation,
            [name]: {
              prestine: false,
              invalid: false,
              message: "",
            },
          },
        });
        break;
      default:
        setExpenseData({
          ...expenseData,
          [name]: value,
        });
        break;
    }
  };

  const createExpense = async (e) => {
    e.preventDefault();
    var response = await CreateExpense(expenseData);
    if (!response.isSuccess) {
      setNotofocation({
        message: "An error occurred while updating an expense  .",
        isError: true,
      });
      return;
    }
    const list = await GetExpenses(expenses.currentPage);
    setExpenses({ ...list.payload });
    setPageList(getPages(list.payload.totalPages));
    setNotofocation({
      message: "Exepnse added successfully .",
      isError: false,
    });
    setShowModal(!showModal);
  };

  const handleEdit = (expenseId) => {
    const expenseRecord = expenses.data.find((e) => e.expenseId === expenseId);
    setExpenseData({
      ...expenseRecord,
      purchasedAt: Date.parse(expenseRecord.purchasedAt),
      createUpdateText: "Update",
      validation: { ...initialExpenseData.validation },
    });
    setShowModal(true);
  };

  const handlePageClick = async (page) => {
    const list = await GetExpenses(page);
    setExpenses({ ...list.payload });
    setPageList(getPages(list.payload.totalPages));
  };

  const handleDelete = async (expenseId) => {
    const res = await DeleteExpense(expenseId);
    if (!res.isSuccess) {
      setNotofocation({
        message: "Error while removing an expense .",
        isError: true,
      });
      return;
    }
    const list = await GetExpenses(expenses.currentPage);
    setExpenses({ ...list.payload });
    setPageList(getPages(list.payload.totalPages));
    setNotofocation({
      message: "An expense record removed succesfully",
      isError: false,
    });
  };

  const getPages = (totalPages) => {
    const pages = [];
    for (let index = 0; index < totalPages; index++) {
      pages.push(index + 1);
    }
    return pages;
  };

  useEffect(() => {
    const source = Token.source();

    (async () => {
      const res = await GetCategories();
      setCategories(res.payload);
      const list = await GetExpenses(expenses.currentPage);
      setExpenses({ ...list.payload });
      setPageList(getPages(list.payload.totalPages));
    })();

    return () => {
      source.cancel();
    };
  }, [expenses.currentPage]);

  return (
    <div
      className="container mt-1 shadow p-3 mb-1 bg-white rounded"
      style={{
        height: "800px",
      }}
    >
      <div className="container">
        {notification.message && (
          <div
            className={`alert alert-${
              notification.isError ? "danger" : "success"
            } alert-dismissible fade show`}
            role="alert"
          >
            <strong>{notification.isError ? "Error" : "Success"}:</strong>{" "}
            {notification.message}
            <button
              onClick={() => setNotofocation({ message: "" })}
              type="button"
              className="close"
              data-dismiss="alert"
              aria-label="Close"
            >
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
        )}
        <div className="row border-bottom ">
          <table className="table table-borderless">
            <tbody>
              <tr>
                <td>Total Spend:$ 1500</td>
                <td>Top category : Groecery</td>
                <td>
                  <span
                    className="edit"
                    onClick={() => {
                      setExpenseData(initialExpenseData);
                      setShowModal(!showModal);
                    }}
                  >
                    Create : <i className="fas fa-plus"></i>
                  </span>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
        <div className="row border-bottom justify-content-center ">
          <ListExpense
            expenses={expenses.data}
            handleDelete={handleDelete}
            handleEdit={handleEdit}
            categories={categories}
          />
        </div>
        <div className="row justify-content-center mt-3">
          <Pagination
            pageList={pageList}
            handlePageClick={handlePageClick}
            currentPage={expenses.currentPage}
          />
        </div>
        {/* Expense modal place holder  */}
        <div>
          <AddExpense
            {...expenseData}
            show={showModal}
            handleClose={handleClose}
            handleChange={handleExpenseChange}
            handleSubmit={createExpense}
            handleDateChange={handleDateChange}
            categories={categories}
          />
        </div>
      </div>
    </div>
  );
}
