import React, { useState } from "react";
import Login from "../components/Login";
import { useHistory } from "react-router-dom";
import { Login as AuthLogin } from "../services/AuthService";
import { isEmail } from "../utilities/utilities";

const initialState = {
  email: "",
  password: "",
  isValidEmail: false,
  errorMessage: "",
};

const LoginContainer = () => {
  const [loginForm, setLoginForm] = useState(initialState);
  const history = useHistory();

  const handleSubmit = async (e) => {
    e.preventDefault();
    const response = await AuthLogin(loginForm.email, loginForm.password);
    if (response.isSuccess) {
      history.push("/home");
    } else {
      setLoginForm({
        ...loginForm,
        errorMessage:
          "Email or password is wrong .Reset the password or contract system administrator",
      });
    }
  };

  const handleChange = (e) => {
    const { name, value } = e.target;

    switch (name) {
      case "email":
        setLoginForm({
          ...loginForm,
          [name]: value,
          isValidEmail: isEmail(value),
        });
        break;
      default:
        setLoginForm({ ...loginForm, [name]: value });
        break;
    }
  };

  const setErrorAlert = () => {
    setTimeout(() => {
      setLoginForm(initialState);
    }, 5000);
    return null; //It will stop the timeout number being displayed on screen
  };

  return (
    <>
      {loginForm.errorMessage && setErrorAlert()}
      <div
        className="container mt-1 shadow p-3 mb-1 bg-white rounded"
        style={{
          height: "750px",
        }}
      >
        <div className="row ">
          <div className="col-md ">
            <nav className="navbar navbar-dark  bg-danger border-bottom navigation">
              <span className="navbar-brand mb-0 h1">
                <i className="fas fa-home fa-1x"></i>asiana
              </span>
            </nav>
          </div>
        </div>
        {/* Login page */}
        <div
          style={{
            height: "550px",
          }}
        >
          <div className="row mt-1 ">
            <div className="col"></div>
            <div className="col-6 mb-5 mt-5">
              <div className="row mb-3">
                <div className="col-12">
                  <Login
                    {...loginForm}
                    handleChange={handleChange}
                    handleSubmit={handleSubmit}
                  />
                </div>
              </div>
              <div className="row mt-5 border-top">
                <div className="col">
                  <small>
                    <a style={{ color: "#f01f0e" }} href="/forgot-password">
                      {" "}
                      Forgot Password
                    </a>
                  </small>
                </div>

                <div className="col">
                  <small>
                    Don't have an account?
                    <a href="/signup" style={{ color: "#f01f0e" }}>
                      {" "}
                      Sign up
                    </a>
                  </small>
                </div>
              </div>
            </div>
            <div className="col"></div>
          </div>

          {/* Social login section */}
        </div>
        {loginForm.errorMessage && (
          <div className="alert alert-danger" role="alert">
            <strong>Oh snap!</strong> {loginForm.errorMessage}
          </div>
        )}
        <div>
          <div className="border-top mt-1  navigation">
            <footer
              className="page-footer font-small blue"
              style={{
                color: "red",
              }}
            >
              <div className="footer-copyright text-center py-3">
                <small>© 2020: codewithnayak.com</small>
              </div>
            </footer>
          </div>
        </div>
      </div>
    </>
  );
};

export default LoginContainer;
